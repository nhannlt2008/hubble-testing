const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';

export default {
  light: {
    text: '#000',
    background: '#fff',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
  },
  dark: {
    text: '#fff',
    background: '#000',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
  },

  white: '#fff',
  black: '#000',
  greyLighter: '#dcdcdc',
  grey: '#767676',
  blue: '#016aa9',
  orange: '#ff9b1a',
  green: '#00FF7F',
};