import {Pay_Loan, Update_Loan, Send_Data, End_Sending} from '../action/actionload';
import { ToastAndroid, AlertIOS, Platform} from 'react-native';

import { createStore, applyMiddleware } from 'redux';

async function sendRequestPayMoney(week) {
    return await fetch('https://www.google.com/search?q=secret+sauce');
  }

async function takeDataUser(username,password) {
    const reponse =  await fetch('https://reqbin.com/echo/post/json', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: username,
        password: password,
      })
    })
    return reponse;
  }

function payLoan (amountWeek){
    return {
        type: Pay_Loan,
        amountWeek: amountWeek,
    }
}

function payLoanUpdate(data){
    return {
        type: Update_Loan,
        data: data,
    }
}
export function StartSend(value){ 
  if (value){
    return {
      type: Send_Data,
    }
  } else {
    return {
      type: End_Sending,
    }
  }
}
export function getDataUser(userName,Password) {
    return function(dispatch) {
      return takeDataUser(userName,Password).then(response => response.json())
      .then(data => {
          console.log(data);
          var newdata = dummydata();
          console.log("OK2");
          dispatch(payLoanUpdate(newdata));
      })
    };
  }
export function pushPayToServer(week) {
    return function(dispatch) {
      return sendRequestPayMoney(week).then(
        (data) => { 
            // console.log(data);
            //check if data is oke then
            dispatch(payLoan(week));
            dispatch(StartSend(false));
            if (Platform.OS === 'android') {
              ToastAndroid.show("Done send request paying", ToastAndroid.SHORT)
            } else {
              AlertIOS.alert("Done send request paying");
            }
        },
        (error) => {
          console.log("Fail");
          dispatch(StartSend(false));
          if (Platform.OS === 'android') {
            ToastAndroid.show("Error connect to server", ToastAndroid.SHORT)
          } else {
            AlertIOS.alert("Error connect to server");
          }
        }
      );
    };
  }

function dummydata(){
  return {
    TotalPayLoan : 3600,
    AmountPayLoan : 300,
    TotalMoneyRemain: 1800, 
    TimePay:6,
    totalTimes: 12,
    ListPayLoan :[{
        loanId: 'L100X240322',
        date: '20-10-2020',
        price: 300,
        times: 6,
      },
      {
        loanId: 'L100X240322',
        date: '20-10-2020',
        price: 300,
        times: 5,
      },
      {
        loanId: 'L100X240322',
        date: '20-10-2020',
        price: 600,
        times: 4,
      },
      {
        loanId: 'L100X240322',
        date: '20-10-2020',
        price: 300,
        times: 2,
      },
      {
        loanId: 'L100X240322',
        date: '20-10-2020',
        price: 300,
        times: 1,
      }]
  } 
}