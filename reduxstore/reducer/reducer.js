import {Pay_Loan, Update_Loan, Send_Data, End_Sending} from '../action/actionload';
const initialState = {
    TotalPayLoan : 0,
    AmountPayLoan : 0,
    TotalMoneyRemain: 0, 
    ListPayLoan :[],
    TimePay: 0,
    sendPayLoad: false,
}
export const payLoanReducer = (state = initialState,action)=>{
    switch(action.type){
        case Pay_Loan:
            console.log(state.TimePay);
            var dateObj = new Date();
            var month = dateObj.getUTCMonth() + 1; //months from 1-12
            var day = dateObj.getUTCDate();
            var year = dateObj.getUTCFullYear();

            newdate = day + "/" + month + "/" + year ;
            const newData = {
                    loanId: Math.floor(Math.random() * 100000) + 1 ,
                    date: newdate,
                    price: state.AmountPayLoan * action.amountWeek,
                    times: state.TimePay + action.amountWeek,
                    }
            const newList = [newData].concat(state.ListPayLoan);
            return {
                ...state,
                ListPayLoan: newList,
                TotalMoneyRemain: state.TotalMoneyRemain -  state.AmountPayLoan * action.amountWeek,
                TimePay: state.TimePay + action.amountWeek,
            };
        case Update_Loan:
            const value = action.data;
            const newlist = value.ListPayLoan;
            // console.log(newlist);
            return {
                ...state,
                TotalPayLoan:value.TotalPayLoan,
                TimePay:value.TimePay,
                AmountPayLoan:value.AmountPayLoan,
                TotalMoneyRemain:value.TotalMoneyRemain,
                ListPayLoan: newlist,
                totalTimes: value.totalTimes,
              }
        case Send_Data:
            return {
                ...state,
                sendPayLoad:true,
            }
        case End_Sending:
            return {
                ...state,
                sendPayLoad:false,
            }
        default: return state;
    }
}
