import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, FlatList } from 'react-native';

import Colors from '../constants/Colors';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import * as action from '../reduxstore/index/index';
import { useDispatch, useSelector } from "react-redux";

interface LoanItem {
  loanId?: string,
  date?: string,
  price?: number,
}

export default function TabOneScreen() {
  const Total = useSelector(state => state.TotalPayLoan);
  const ListPay = useSelector(state => state.ListPayLoan);
  const moneyPerWeek = useSelector(state => state.AmountPayLoan);
  const totalTimes =  useSelector(state => state.totalTimes);
  const dispatch = useDispatch();
  const [OneTime, setOneTime] = useState(false);
    useEffect(() => {    
      if( OneTime == false) {
        dispatch(action.getDataUser("Nhannlt2008","123412345"));
        console.log("OK");
        console.log(OneTime);
      }
      setOneTime(true);

  });
  const renderLoanItem = ({ item }) => {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 12, alignItems: 'center' }}>
        <View>
          <Text style={styles.loanId}>{item?.loanId}</Text>
          {(item?.times == totalTimes) ? 
          <Text style={styles.loanTimesLast}>{item?.times}/{totalTimes}</Text>
          : <Text style={styles.loanTimes}>{item?.times}/{totalTimes}</Text>}

        </View>

        <View>
          <Text style={styles.loanPrice}>$ {item?.price}</Text>
          <Text style={styles.loanDate}>{item?.date}</Text>
        </View>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.logoWrapper}>
        <Image
          resizeMode="center"
          source={{ uri: 'https://images.glints.com/unsafe/1200x0/glints-dashboard.s3.amazonaws.com/company-logo/b870110af78209c093b78b521bde971c.png' }}
          style={styles.logoStyle}
        />
        <Text style={styles.title}>Loan Information</Text>
      </View>
      <View style={styles.headerContentContainer}>
        <Text style={styles.contentHeader}>Total Amount Money:</Text>
        <Text style={styles.valueHeader}>{Total}</Text>
      </View>
      <View style={styles.headerContentContainer}>
        <Text style={styles.contentHeader}>Your Weekly Repayment: </Text>
        <Text style={styles.valueHeader}>{moneyPerWeek}</Text>
      </View>
      <View style={styles.headerContentContainer}>
        <Text style={styles.contentHeader}>Your Loan Term: </Text>
        <Text style={styles.valueHeader}>29-03-2021</Text>
      </View>
      {/* <View style={styles.headerContentContainer}>
        <Text style={styles.contentHeader}>Your Next Pay Date: </Text>
        <Text style={[styles.valueHeader, { color: Colors.orange }]}>33-03-2021</Text>
      </View> */}
      <View style={styles.headerContentContainer}>
        <Text style={styles.contentHeader}>Recently Loan Repayment</Text>
      </View>
      <View style={styles.separator} />
      <FlatList
        showsVerticalScrollIndicator={false}
        style={{ marginHorizontal: 36 }}
        ItemSeparatorComponent={() => <View style={styles.separatorList} />}
        data={ListPay}
        renderItem={(item) => renderLoanItem(item)}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logoWrapper: {
    alignItems: 'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 14
  },
  headerContentContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginHorizontal: 36,
    justifyContent: 'space-between',
    width: '80%',
    alignItems: 'center'
  },
  contentHeader: {
    fontSize: 14
  },
  valueHeader: {
    color: Colors.blue,
    fontSize: 18
  },
  separator: {
    marginVertical: 10,
    height: 1,
    width: '80%',
    marginLeft: 36,
    backgroundColor: Colors.greyLighter
  },
  separatorList: {
    marginVertical: 10,
    height: 1,
    width: '80%',
    marginLeft: 36,
    backgroundColor: Colors.greyLighter
  },
  logoStyle: {
    height: 80,
    width: 60,
    marginTop: 60
  },
  loanId: {
    color: Colors.grey
  },
  loanDate: {
    color: Colors.grey
  },
  loanPrice: {
    color: Colors.black,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold'
  },
  loanTimes: {
    color: Colors.orange,
  },
  loanTimesLast: {
    color: Colors.green,
  }
});