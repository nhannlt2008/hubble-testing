import React, { useState, useEffect } from 'react';
import { StyleSheet, ActivityIndicator, ToastAndroid, AlertIOS, Platform} from 'react-native';

import { Text, View } from '../components/Themed';
import { useDispatch, useSelector } from "react-redux";
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as action from '../reduxstore/index/index';
import Colors from '../constants/Colors';
import { FontAwesome5 } from '@expo/vector-icons';

export default function TabTwoScreen() {
  const TotalMoneyRemain = useSelector(state => state.TotalMoneyRemain);
  const MoneyPerWeek = useSelector(state => state.AmountPayLoan);
  const sendPayLoad = useSelector(state => state.sendPayLoad);

  
  const dispatch = useDispatch();
  const [week, setWeek] = useState(1);
  useEffect(() => {    // Update the document title using the browser API   

  });
  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        Total Repay Need: {TotalMoneyRemain == -1 ? '0' : TotalMoneyRemain} {'\n'}</Text>
      <Text style={styles.title}>Week paying:{'\n'}</Text>

      <View style={{ flexDirection: 'row', marginVertical: 24 }}>
        <TouchableOpacity onPress={() => { if (week > 1) { setWeek(week - 1) } }}>
          <FontAwesome5 name="minus" size={24} color={Colors.blue} />
        </TouchableOpacity>
        <Text style={styles.title}>{week}</Text>
        <TouchableOpacity onPress={() => { if ((week + 1) * MoneyPerWeek <= TotalMoneyRemain) { setWeek(week + 1) } }}>
          <FontAwesome5 name="plus" size={24} color={Colors.blue} />
        </TouchableOpacity>
      </View>
      <Text>Total MoneyPay: {week * MoneyPerWeek}</Text>

      <TouchableOpacity style={styles.button} onPress={() => {
          if (sendPayLoad == false) {
            // setSending(true);
            if (week * MoneyPerWeek <= TotalMoneyRemain) {
              dispatch(action.StartSend(true));
              //dialog Show Send to server.
              dispatch(action.pushPayToServer(week));
              setWeek(1);
            } else {
              if (Platform.OS === 'android') {
                ToastAndroid.show("You have pay all the check", ToastAndroid.SHORT)
              } else {
                AlertIOS.alert("You have pay all the check");
              }
              //Tell Them You Pay Enought.
            }
          }
        }
      }
        >
          {(sendPayLoad == true) ? <ActivityIndicator size="large" color={Colors.white} />
          :
          <Text style={styles.btnText}>
            Repay Money
            </Text>
          }
      </TouchableOpacity>
    </View>
  );
}
// const mapDispatchToProps = { payLoan };
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginHorizontal: 26
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  btnText: {
    color: Colors.white,
    fontSize: 16
  },
  button: {
    marginTop: 40,
    backgroundColor: Colors.blue,
    paddingHorizontal: 36,
    paddingVertical: 12,
    borderRadius: 12
  }
});
